package labs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@SpringBootApplication
public class PetShop {
    public static void main(String[] Args)throws SQLException {
        Connection conn = DriverManager.getConnection("jdbc:h2:~/PetShopDB/PetShopDB", "sa", "");
    /*    PreparedStatement ps = conn.prepareStatement("CREATE TABLE Items (Id int PRIMARY KEY, Name VARCHAR, Price double, IsPet bit, Age double)");
        ps.execute();

        ps = conn.prepareStatement("INSERT INTO Items(Name, Price, IsPet, Age) VALUES ('Haski',100, 1, 2),('Collar', 1, 0, null),('Bulldog',90, 1, 1)");
        ps.execute();


        ps = conn.prepareStatement("CREATE TABLE Users (Id int PRIMARY KEY auto_increment, FirstName varchar, LastName varchar, Password varchar)");
        ps.execute();

        ps = conn.prepareStatement("INSERT INTO Users(FirstName, LastName, Password) VALUES ('Aleksey', 'Kachurin', 'qwerty')");
        ps.execute();*/

      /*  PreparedStatement ps = conn.prepareStatement("CREATE TABLE Carts (Id int PRIMARY KEY, UserId int, ItemId int)");
        ps.execute();
        ps = conn.prepareStatement("ALTER TABLE Carts  ADD FOREIGN KEY (UserId) references Users(Id)");
        ps.execute();
        ps = conn.prepareStatement("ALTER TABLE Carts  ADD FOREIGN KEY (ItemId) references Items(Id)");
        ps.execute();*/
      /*  ps = conn.prepareStatement("CREATE TABLE Balance (UserId int PRIMARY KEY, Money double)" );
        ps.execute();
        ps = conn.prepareStatement("Insert into Balance (UserId, Money) VALUES (1, 100000)");
        ps.execute();

        ps = conn.prepareStatement("update Balance set Money = 1000 where UserId = 1");
        ps.execute();*/
        //org.h2.tools.Server.startWebServer(conn);
        SpringApplication.run(PetShop.class); }

}
