package labs.Item;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
@Component
public class ItemDAO {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ItemDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    public List<Item> findAll()
    {
        return jdbcTemplate.query("select * from Items", new PetRowMapper());
    }
    public void addItem(Item item)
    {
        byte isPet;
        if (item.getType() == Item.Types.Pet)
        {
            isPet = 1;
        }
        else isPet = 0;
        jdbcTemplate.execute("INSERT INTO Items(Id, Name, Price, IsPet, Age) VALUES (" + item.getId() + " ,'" + item.getName() + "', "+ item.getCost() + ","+ isPet + "," + item.getAge()+")");
    }
    public void removeItem(Item item)
    {
        jdbcTemplate.execute("Delete from Items where Id = " + item.getId());
    }

    private class PetRowMapper implements RowMapper<Item> {

        @Override
        public Item mapRow(ResultSet resultSet, int i) throws SQLException {
            int id = resultSet.getInt("Id");
            String petName = resultSet.getString("Name");
            Item.Types types;
            boolean tmp = resultSet.getBoolean("isPet");
            if (tmp == true)
                types = Item.Types.Pet;
            else types = Item.Types.Staff;
            int price = resultSet.getInt("price");
            int age = resultSet.getInt("Age");

            return new Item(id, petName, types, age, price);
        }
    }
}
