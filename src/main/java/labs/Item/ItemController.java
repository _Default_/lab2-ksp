package labs.Item;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;

@Controller
public class ItemController {

    private final ItemService p;

    @Autowired
    public ItemController(ItemService p) {
        this.p = p;
    }

    @RequestMapping(value="items/pets", method = RequestMethod.GET)
    @ResponseBody
    public String getPets()
    {
        JSONObject response = new JSONObject();
        List<Item> resp = p.getPets();
        if(!resp.isEmpty())
        {
            response.put("Status", "Запрос выполен");
            response.put("Response", resp);
            return response.toString();
        }
        response.put("Status", "Error 10");
        response.put("Response", "Нет товаров в данного типа продаже.");
        return response.toString();
    }

    @RequestMapping(value="items/staffs", method = RequestMethod.GET)
    @ResponseBody
    public String getStuffs(){
        JSONObject response = new JSONObject();
        List<Item> resp = p.getStuffs();
        if(!resp.isEmpty())
        {
            response.put("Status", "Запрос выполен");
            response.put("Response", resp);
            return response.toString();
        }
        response.put("Status", "Error 11");
        response.put("Response", "Нет товаров данного типа в продаже.");
        return response.toString();
    }

    @RequestMapping(value="items/pets/{id}", method = RequestMethod.GET)
    @ResponseBody
    public String getPets(@PathVariable String id)
    {
        JSONObject response = new JSONObject();
        List<Item> resp = new LinkedList<>();
                resp.add(p.getPets(Integer.parseInt(id)));
                response.put("Status", "Запрос выполнен");
            response.put("Response", resp);
            return response.toString();
    }

    @RequestMapping(value="items/staffs/{id}", method = RequestMethod.GET)
    @ResponseBody
    public String getStuffs(@PathVariable String id) {
        JSONObject response = new JSONObject();
        Item resp = p.getStuffs(Integer.parseInt(id));
        response.put("Status", "Запрос выполнен");
        response.put("Response", resp);
        return response.toString();
    }

    @RequestMapping(value="items/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public String removeItems(@PathVariable("id") String id) {
        JSONObject response = new JSONObject();
        Item item = p.getItems(Integer.parseInt(id));
        boolean wasOK = p.removeItem(item);
        if (wasOK){
            response.put("Status", "Запрос выполнен");
            response.put("Response", "Товар убран из продажи.");
            return response.toString();
        }
        response.put("Status", "Error 100");
        response.put("Response", "Невозможно удалить товар.");
        return response.toString();
    }

    @RequestMapping(value="items/{item}", method = RequestMethod.PUT)
    @ResponseBody
    public String addItem(@PathVariable String item)
    {
        String[] tmp = item.split(" "); //  name, type, age, cost
        Item elem;
        if (tmp[1].equals("Pet")) {
            elem = new Item(0, tmp[0], Item.Types.Pet, Integer.parseInt(tmp[2]), Integer.parseInt(tmp[3]));
        }
        else if (tmp[2].equals("Staff")) {
            elem = new Item(0, tmp[0], Item.Types.Staff, Integer.parseInt(tmp[2]), Integer.parseInt(tmp[3]));
        }
        else elem = null;
        JSONObject response = new JSONObject();
        response.put("Status","Запрос выполнен");
        response.put("", p.addItem(elem));
        return response.toString();
    }

    @RequestMapping(value="items", method = RequestMethod.GET)
    @ResponseBody
    public String getItems() {
        JSONObject response = new JSONObject();
        List<Item> resp = p.getItems();
        if(!resp.isEmpty())
        {
            response.put("Status", "Запрос выполен");
            response.put("Response", resp);
            return response.toString();
        }
        response.put("Status", "Error 11");
        response.put("Response", "Нет товаров в продаже.");
        return response.toString();
    }


}