package labs.Item;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static labs.Item.Item.Types.Pet;
import static labs.Item.Item.Types.Staff;



@Service
public class ItemService {
    ItemDAO itemDAO;
    private int index = 1;
    @Autowired
    ItemService(ItemDAO itemDAO) {
       this.itemDAO = itemDAO;
    }

    public final List<Item> getItems(){
        return itemDAO.findAll();
    }

    public Item getItems(int id) {

        for(Item it : getItems())
            if(it.getId() == id)
            {
                return it;
            }
        return null;
    }

    public final List<Item> getItems(Item.Types type) {
        List<Item> out = new LinkedList<>();
        for(Item it : getItems())
            if(it.getType() == type)
                out.add(it);
        return out;
    }

    public Item getItems(int id, Item.Types type) {
        Item out = getItems(id);
        if(out == null) return null;
        if(out.getType().equals(type))
            return out;
        return null;
    }



    public final List<Item> getPets() {
        return getItems(Pet);
    }

    public final List<Item> getStuffs() {
        return getItems(Staff);
    }

    public Item getPets(int id) {
        return getItems(id, Pet);
    }

    public Item getStuffs(int id) {
        return getItems(id, Staff);
    }

    public boolean addItem(Item item) {
        int i;
        ArrayList<Integer> arrayList= new ArrayList<>();
        int max = 0;
        for (Item it: getItems())
        {
            if (it.getId()>max)
            {
                max = it.getId();
            }
        }
        for (i = 0; i < max; i++)
            arrayList.add(i, 0);
        for (Item it: getItems())
        {
            arrayList.set((it.getId()-1), 1);
        }
        for (i = 0; i < arrayList.size(); i++){
            if (arrayList.get(i)!= 1) {
                index = i + 1;
                break;
            }
            index = max + 1;
            item.setId(item, index);
        }

        itemDAO.addItem(item);
        return true;
    }

    public boolean removeItem(Item item) {
        itemDAO.removeItem(item);
        return true;
    }


}