package labs.Currency;

import labs.Balance.BalanceService;

import labs.Cart.Cart;
import labs.Cart.CartDAO;
import labs.Cart.CartService;
import labs.Item.ItemService;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

@Service
public class CurrencyService {
    CartDAO cartDAO;
    BalanceService balanceService;
    CurrencyService(BalanceService balanceService, CartDAO cartDAO)
    {
        this.balanceService = balanceService;
        this.cartDAO = cartDAO;
    }
     public JSONObject toPay() {
         double totalPrice = cartDAO.getTotalPrice();
         double balance = balanceService.getBalance();
         JSONObject jsonObject = new JSONObject();
         if (balance < totalPrice) {
             jsonObject.put("Status", "Error 5");
             jsonObject.put("Response", "Недостаточно средств");
             return  jsonObject;
         }

         balanceService.set(balance - totalPrice);
         cartDAO.deleteAll();
         jsonObject.put("Status", "Запрос выполнен");
         jsonObject.put("Response", "Товары куплены.");
         return  jsonObject;
    }
}
