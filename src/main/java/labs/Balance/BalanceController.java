package labs.Balance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class BalanceController {

    private BalanceService bs;

    @Autowired
    public BalanceController(BalanceService bs) {
        this.bs = bs;
    }

    @RequestMapping(value="balance", method = RequestMethod.GET, produces = "json/application")
    @ResponseBody
    public  getBalance()
    {
        return bs.get().toString();
    }
    @RequestMapping(value="balance/{coin}", method = RequestMethod.PUT)
    @ResponseBody
    public String setBalance(@PathVariable String coin) {
        return bs.set(Double.parseDouble(coin)).toString();
    }
}