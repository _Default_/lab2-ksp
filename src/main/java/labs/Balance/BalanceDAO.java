package labs.Balance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

@Component
public class BalanceDAO {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public BalanceDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    public List<Balance> getBalanceByUserID(int id) {
        return jdbcTemplate.query("select * from Balance where UserId =" + id, new RowMapper<Balance>() {
            @Override
            public Balance mapRow(ResultSet resultSet, int i) throws SQLException {
                double money = resultSet.getDouble("Money");
                return new Balance(money);
            }
        });
    }

    public void updateUserBalance(double newBalance, int id) {
        List<Integer> ids =  jdbcTemplate.query("select * from Balance", new RowMapper<Integer>() {
            @Override
            public Integer mapRow(ResultSet resultSet, int i) throws SQLException {
                Integer tmpid = resultSet.getInt("UserId");
                return tmpid;
            }
        });
        System.err.println(ids);
        if (ids.contains(id)){
        jdbcTemplate.execute("UPDATE Balance set Money = " + newBalance + " where UserId = " + id);
        }
        else {
            jdbcTemplate.execute("Insert into Balance (UserId, Money) VALUES (" + id + ", " + newBalance +")");
        }
    }
}
