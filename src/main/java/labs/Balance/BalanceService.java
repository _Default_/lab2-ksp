package labs.Balance;


import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BalanceService {
    BalanceDAO balanceDAO;

    @Autowired BalanceService(BalanceDAO balanceDAO)
    {
        this.balanceDAO = balanceDAO;
    }
    public double getBalance()
    {
        return balanceDAO.getBalanceByUserID(1).get(0).getPenny();
    }
    public JSONObject get() {
        JSONObject resp = new JSONObject();
        resp.put("Status", "Запрос выполнен.");
        resp.put("Response", "Баланс: " + balanceDAO.getBalanceByUserID(1).get(0).getPenny());
        return resp;
    }
    public JSONObject set(double money)
    {
        JSONObject resp = new JSONObject();
        resp.put("Status", "Запрос выполнен.");
        resp.put("Response", "Баланс " + money + " установлен.");
        balanceDAO.updateUserBalance(money, 1);
        return resp;
    }
}
