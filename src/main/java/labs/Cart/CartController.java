package labs.Cart;

import labs.Currency.CurrencyService;
import labs.Item.Item;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class CartController {

    private CartService cs;
    CurrencyService currencyService;

    @Autowired
    public CartController(CartService cs, CurrencyService currencyService) {
        this.cs = cs;
        this.currencyService = currencyService;
    }


    @RequestMapping(value="cart/pet/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public String putPet(@PathVariable String id) {
        String response = cs.putItem(Integer.parseInt(id), Item.Types.Pet).toString();
        return (response);
    }

    @RequestMapping(value="cart/staff/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public String putStaff(@PathVariable String id) {
        String response = cs.putItem(Integer.parseInt(id), Item.Types.Staff).toString();
        return (response);
    }

    @RequestMapping(value="cart/item/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public String removeItem(@PathVariable String id) {
        String response = cs.removeItem(Integer.parseInt(id)).toString();
        return response;
    }
    @RequestMapping(value="cart/cartid/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public String removeItemId(@PathVariable String id) {
        String response = cs.removeItemId(Integer.parseInt(id)).toString();
        return response;
    }



    @RequestMapping(value="cart/item", method = RequestMethod.DELETE)
    @ResponseBody
    public String removeItem() {
        cs.removeItem();
        return cs.removeItem().toString();
    }

    @RequestMapping(value="cart/petall", method = RequestMethod.DELETE)
    @ResponseBody
    public String removePets() {
        String response = cs.removeItem(Item.Types.Pet).toString();
        return response;
    }

    @RequestMapping(value="cart/staffall", method = RequestMethod.DELETE)
    @ResponseBody
    public String removeStaffs() {
        String response = cs.removeItem(Item.Types.Staff).toString();
        return response;
    }

    @RequestMapping(value="cart", method = RequestMethod.GET)
    @ResponseBody

    public JSONObject get() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("Предметы в корзине", cs.getCart());
        jsonObject.put("Итого:", cs.sum());
        return jsonObject;
    }
}
