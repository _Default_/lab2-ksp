package labs.Cart;

import labs.Balance.Balance;
import labs.Balance.BalanceService;
import labs.Item.Item;
import labs.Item.ItemService;
import labs.User;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import java.util.*;

import static labs.Item.Item.Types.Pet;
import static labs.Item.Item.Types.Staff;


@Service
public class CartService {

    private ItemService itemService;
    private BalanceService balanceService;
    private CartDAO cartDAO;

    List<Cart> cartL;
    Cart cart;
    User us;
    Balance balance;

    private int index = 1;

    public CartService(ItemService itemService, BalanceService balanceService, CartDAO cartDAO) {
        this.itemService = itemService;
        this.balanceService = balanceService;
        this.cartDAO = cartDAO;

        us = new User(1, User.Role.User, balance, cart);

    }

    public List<Cart> getCart()
    {
        return cartDAO.findAll();
    }

    public Cart getCartElement(int id) {
        for(Cart it : getCart())
            if(it.getInternalID() == id)
                return it;
        return null;
    }

    public void addElement(Cart input) {
        cartDAO.addItem(input.getInternalID(), input.getItemId(), input.getUserId());
    }

    public JSONObject putItem(int id, Item.Types types) {
        JSONObject jsonObject = new JSONObject();
        int i;
        ArrayList<Integer> arrayList= new ArrayList<>();
        int max = 0;
        for (Cart it: getCart())
        {
            if (it.getInternalID()>max)
            {
             max = it.getInternalID();
            }
        }
        for (i = 0; i < max; i++)
            arrayList.add(i, 0);
        for (Cart it: getCart())
        {
            arrayList.set(it.getInternalID()-1, 1);
        }
        for (i = 0; i < arrayList.size(); i++){
            if (arrayList.get(i)!= 1) {
                index = i + 1;
                break;
            }
            index = max + 1;
        }

        if (types == Pet){
        if (itemService.getItems(id, types)!= null) {
            addElement(new Cart(index, 1, id));
            jsonObject.put("Status", "Запрос выполнен.");
            jsonObject.put("Response","Питомец с id " + id + " успешно добавлен.");
            return jsonObject;
        }
        else {
            jsonObject.put("Status", "Error 1");
            jsonObject.put("Message","Данный Id не принадлежит питомцу.");
            return jsonObject;
        }
        }
        if (types == Staff){
            if (itemService.getItems(id, types)!= null) {
                addElement(new Cart(index,1, id));
                jsonObject.put("Status", "Запрос выполнен.");
                jsonObject.put("Response","Товар с id " + id + " успешно добавлен.");
                return jsonObject;
            }
            else {
                jsonObject.put("Status", "Error 1");
                jsonObject.put("Message","Данный Id не принадлежит товару.");
                return jsonObject;
            }}
        return new JSONObject();
    }

    public JSONObject removeItem(int id) {
        JSONObject response = new JSONObject();
        boolean flag = false;
        for (Cart it: getCart())
        {
            if (it.getItemId() == id)
                flag = true;
        }
        if (flag == true) {
            cartDAO.deleteItem(id);
            response.put("Status", "Запрос выполнен");
            response.put("Response", "Товар с id "+ id + " удален из корзины.");
            return response;
        }
        response.put("Status", "Error 2");
        response.put("Response", "Товар с id "+ id + " отсутствует в корзине.");
        return response;
    }

    public JSONObject removeItemId(int id) {
        JSONObject response = new JSONObject();
        boolean flag = false;
        for (Cart it: getCart())
        {
            if (it.getInternalID() == id)
                flag = true;
        }
        if (flag == true) {
            cartDAO.deleteItemId(id);
            response.put("Status", "Запрос выполнен");
            response.put("Response", "Позиция "+ id + " удалена из корзины.");
            return response;
        }
        response.put("Status", "Error 3");
        response.put("Response", "Позиция "+ id + " отсутствует в корзине.");
        return response;
    }

    public JSONObject removeItem(Item.Types types) {
        JSONObject response = new JSONObject();
        boolean flag = false;
        for (Cart it: getCart())
        {
            if (itemService.getItems(it.getItemId()).getType() == types)
                flag = true;
        }
        if (flag == true) {
            cartDAO.deleteItemType(types);
            response.put("Status", "Запрос выполнен");
            response.put("Response", "Товары с типом "+ types + " удален из корзины.");
            return response;
        }
        response.put("Status", "Error 4");
        response.put("Response", "Товар с типом "+ types + " отсутствует в корзине.");
        return response;
    }

    public JSONObject removeItem() {
        JSONObject response = new JSONObject();
        response.put("Status", "Запрос выполнен");
        response.put("Response", "Корзина очищена.");
        cartDAO.deleteAll();
        return response;
    }
    double sum()
    {
        return cartDAO.getTotalPrice();
    }
}