package labs.Cart;

import labs.Balance.Balance;
import labs.Item.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Component
public class CartDAO {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public CartDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Cart> findAll() {
        return jdbcTemplate.query("select * from Carts", new RowMapper<Cart>(){

            @Override
            public Cart mapRow(ResultSet resultSet, int i) throws SQLException {
                int id = resultSet.getInt("id");
                int userId = resultSet.getInt("userId");
                int itemId = resultSet.getInt("itemId");
                return new Cart(id ,userId, itemId);
            }
        });
    }

    public void addItem(int id, int itemId, int userId) {
        jdbcTemplate.execute("insert into Carts(Id, UserID, ItemId) values( " + id + ", " + userId +", "  + itemId + ")");
    }

    public void deleteItem(int id) {
        jdbcTemplate.execute("delete from Carts where itemId =" + id);
    }
    public void deleteItemId(int id) {
        jdbcTemplate.execute("delete from Carts where Id =" + id);
    }
    public void deleteItemType (Item.Types types)
    {
        boolean tmp;
        if (types == Item.Types.Pet){ tmp = true;}
        else tmp = false;
        jdbcTemplate.execute("Delete from Carts where ITEMID IN (Select  Id from Items Where ISPET  = " + tmp +")");
    }
    public void deleteAll() {
        jdbcTemplate.execute("delete from Carts");
    }
    public double getTotalPrice() {
        String query = "select sum(i.Price) as totalPrice from Carts c left join Items i on c.ItemID = i.ID ";

        List<Balance> totalPrice = jdbcTemplate.query(query, new RowMapper<Balance>() {

            @Override
            public Balance mapRow(ResultSet resultSet, int i) throws SQLException {
                return new Balance(resultSet.getDouble("totalPrice"));
            }
        });

        return totalPrice.get(0).getPenny();
    }

}
