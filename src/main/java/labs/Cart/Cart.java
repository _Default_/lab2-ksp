package labs.Cart;



public class Cart {
    private int internalID;
    private int userId;
    private int itemId;

    public Cart(int internalID, int userId, int itemId) {
        this.internalID = internalID;
        this.userId = userId;
        this.itemId = itemId;
    }

    public int getInternalID(){return internalID;}
    public int getUserId(){return userId;}
    public int getItemId(){return itemId;}
}

